##############################################################
# Homework #4
# name: Janmejay Bhavsar
# sbuid: 110476847
##############################################################
.text

##############################
# PART 1 FUNCTIONS
##############################

clear_board:
    #Define your code here
	############################################
	addi $sp, $sp, -24
	sw $s0, 0($sp)
	sw $s1, 4($sp)
	sw $s2, 8($sp)
	sw $s3, 12($sp)
	sw $s4, 16($sp)
	sw $ra, 20($sp)
	move $s0, $a0 #address of the 2d matrix
	move $s1, $a1 #num_rows
	move $s2, $a2 #num_cols
	blt $s1, 2, invalid #invalid
	blt $s2, 2, invalid #invalid
	li $s3, 0 #row counter
	li $t0, -1 #fill the board with -1
	li $t3, 2 #to multiply by 2
rowLoop:
	li $s4, 0 #column counter
colLoop:
	mul $t1, $s3, $s2 #i*num_cols
	add $t1, $t1, $s4 #i*num_cols+j
	mul $t1, $t1, $t3 #4*(i*num_cols+j)
	add $t1, $t1, $s0 #address of 2d matrix+4*(i*num_cols+j)
	sh $t0, 0($t1) #put -1 in all cells of the matrix
	addi $s4, $s4, 1 #col counter increment
	blt $s4, $s2, colLoop
colLoopDone:
	addi $s3, $s3, 1 #row counter increment
	blt $s3, $s1, rowLoop
rowLoopDone:
	li $v0, 0 #success
	j done	
invalid:
	li $v0, -1 #return -1
	############################################
done:
	lw $s0, 0($sp)
	lw $s1, 4($sp)
	lw $s2, 8($sp)
	lw $s3, 12($sp)
	lw $s4, 16($sp)
	lw $ra, 20($sp)
	addi $sp, $sp, 24
	jr $ra

place:
    #Define your code here
	############################################
	#addi $sp, $sp, 12 #first set the sp to og position
	addi $sp, $sp, -28 #ready to store the reg
	sw $s0, 0($sp)
	sw $s1, 4($sp)
	sw $s2, 8($sp)
	sw $s3, 12($sp)
	sw $s4, 16($sp)
	sw $s5, 20($sp)
	sw $ra, 24($sp)
	move $s0, $a0 #base address
	move $s1, $a1 #n_rows
	move $s2, $a2 #n_cols
	move $s3, $a3 #row
	addi $sp, $sp, 28 #now add to take the args
	lw $s4, 0($sp) #col
	lw $s5, 4($sp) #val
	addi $sp, $sp, -28 #now again go back
	li $t1, 2 #to multiply
	blt $s1, 2, invalid1
	blt $s2, 2, invalid1
	addi $t0, $s1, -1
	blt $s3, 0, invalid1
	bgt $s3, $t0, invalid1
	addi $t0, $s2, -1
	blt $s4, 0, invalid1
	bgt $s4, $t0, invalid1
	bge $s5, 2, checkPowerOf2
	beq $s5, -1, store
	j invalid1
checkPowerOf2:
	addi $t2, $s5, -1
	and $t2, $t2, $s5
	bnez $t2, invalid1
store:
	mul $t0, $s3, $s2 #i*n_cols
	add $t0, $t0, $s4 #i*n_cols+j
	mul $t0, $t0, $t1 #2*(i*n_cols+j)
	add $t0, $t0, $s0 #base_address+2*(i*n_cols+j)
	sh $s5, 0($t0) #place the val
	li $v0, 0
	j donePlace
invalid1:
	li $v0, -1
donePlace:
	lw $s0, 0($sp)
	lw $s1, 4($sp)
	lw $s2, 8($sp)
	lw $s3, 12($sp)
	lw $s4, 16($sp)
	lw $s5, 20($sp)
	lw $ra, 24($sp)
	addi $sp, $sp, 28
	############################################
    jr $ra

start_game:
    #Define your code here
	############################################
	#addi $sp, $sp, 12
	addi $sp, $sp, -32
	sw $s0, 0($sp)
	sw $s1, 4($sp)
	sw $s2, 8($sp)
	sw $s3, 12($sp)
	sw $s4, 16($sp)
	sw $s5, 20($sp)
	sw $s6, 24($sp)
	sw $ra, 28($sp)
	move $s0, $a0 #base address
	move $s1, $a1 #num_rows
	move $s2, $a2 #num_cols
	move $s3, $a3 #r1
	addi $sp, $sp, 32
	lw $s4, 0($sp) #c1
	lw $s5, 4($sp) #r2
	lw $s6, 8($sp) #c2
	addi $sp, $sp, -32
	blt $s1, 2, invalid2
	blt $s2, 2, invalid2
	addi $t1, $s1, -1
	addi $t2, $s2, -1
	bltz $s3, invalid2
	bltz $s4, invalid2
	bltz $s5, invalid2
	bltz $s6, invalid2
	bgt $s3, $t1, invalid2
	bgt $s4, $t2, invalid2
	bgt $s5, $t1, invalid2
	bgt $s6, $t2, invalid2
init:
	jal clear_board
	beq $v0, 0, continueStart
continueStart:
	addi $sp, $sp, -8
	sw $s4, 0($sp)
	li $t0, 2
	sw $t0, 4($sp)
	jal place
	addi $sp, $sp, 8
	beq $v0, 0, continueAgain
continueAgain:
	move $a3, $s5
	addi $sp, $sp, -8
	sw $s6, 0($sp)
	li $t0, 2
	sw $t0, 4($sp)
	jal place
	addi $sp, $sp, 8
	beq $v0, 0, success
success:
	li $v0, 0
	j doneStart
invalid2:
	li $v0, -1
doneStart:
	lw $s0, 0($sp)
	lw $s1, 4($sp)
	lw $s2, 8($sp)
	lw $s3, 12($sp)
	lw $s4, 16($sp)
	lw $s5, 20($sp)
	lw $s6, 24($sp)
	lw $ra, 28($sp)
	addi $sp, $sp, 32
	############################################
    jr $ra

##############################
# PART 2 FUNCTIONS
##############################

merge_row:
    #Define your code here
    ############################################
    #addi $sp, $sp, 4
    addi $sp, $sp, -36
    sw $s0, 0($sp)
    sw $s1, 4($sp)
    sw $s2, 8($sp)
    sw $s3, 12($sp)
    sw $s4, 16($sp)
    sw $s5, 20($sp)
    sw $s6, 24($sp)
    sw $s7, 28($sp)
    sw $ra, 32($sp)
    move $s0, $a0 #base address
    move $s1, $a1 #num_rows
    move $s2, $a2 #num_cols
    #addi $s2, $s2, -1 #cols-1
    move $s3, $a3 #row
    addi $sp, $sp, 36
    lw $s4, 0($sp) #direction
    addi $sp, $sp, -36
    addi $t8, $s1, -1
    bltz $s3, invalid3
    bgt $s3, $t8, invalid3
    blt $s1, 2, invalid3
    blt $s2, 2, invalid3
    li $t1, 2 #to multiply
    li $t7, -1 #to place into the blank cell
    #li $s5, 0 #row counter
    li $s7, 0 #non zero cells counter
    beqz $s4, rowLoop1
    beq $s4, 1, rowLop
    j invalid3
rowLoop1:
	li $s6, 0 #column counter
colLoop1:
	mul $t0, $s3, $s2 #i * num_columns
	add $t0, $t0, $s6 #i * num_columns + j
	mul $t0, $t0, $t1 # 2*(i * num_columns + j)
	add $t0, $t0, $s0 #base_addr + 2*(i * num_columns + j)
	lh $t3, 0($t0) #number at arr[i][j]
	addi $t4, $s6, 1 #j+1
	mul $t5, $s3, $s2 #i * num_columns
	add $t5, $t5, $t4 #i * num_columns + j
	mul $t5, $t5, $t1 # 2*(i * num_columns + j)
	add $t5, $t5, $s0 #base_addr + 2*(i * num_columns + j)
	lh $t6, 0($t5) #number at arr[i][j+1] 
	beq $t3, $t6, mergeLeft
	j doneMergeLeft
mergeLeft:
	bne $t3, -1, goAhead
	j doneMergeLeft
goAhead:
	add $t3, $t3, $t6 #double the number
	sh $t3, 0($t0)	#store it in the address of arr[i][j]
	sh $t7, 0($t5) #place -1 at position of arr[i][j+1]
	j doneMergeLeft
doneMergeLeft:
	addi $s6, $s6, 1 #j++
	blt $s6, $s2, colLoop1
	j colLoopDone1
rowLop:
	addi $s6, $s2, -1
colLop:
	mul $t0, $s3, $s2 #i * num_columns
	add $t0, $t0, $s6 #i * num_columns + j
	mul $t0, $t0, $t1 # 2*(i * num_columns + j)
	add $t0, $t0, $s0 #base_addr + 2*(i * num_columns + j)
	lh $t3, 0($t0) #number at arr[i][j]
	addi $t4, $s6, -1 #j-1
	mul $t5, $s3, $s2 #i * num_columns
	add $t5, $t5, $t4 #i * num_columns + j
	mul $t5, $t5, $t1 # 2*(i * num_columns + j)
	add $t5, $t5, $s0 #base_addr + 2*(i * num_columns + j)
	lh $t6, 0($t5) #number at arr[i][j-1] 
	beq $t3, $t6, mergeRight
	j doneMergeRight
mergeRight:
	bne $t3, -1, goAhead1
	j doneMergeRight
goAhead1:
	add $t3, $t3, $t6 #double the number
	sh $t7, 0($t5)	#place the doubled number at position of arr[i][j]
	sh $t3, 0($t0) #place -1 at position of arr[i][j-1]
	j doneMergeRight
doneMergeRight:
	addi $s6, $s6, -1 #j--
	bgtz $s6, colLop
	j colLoopDone1
invalid3:
	li $v0, -1
	j doneMerge
colLoopDone1:
	li $t0, 0 #col counter
colLop3:
	mul $t2, $s3, $s2 #i * num_cols
	add $t2, $t2, $t0 #i * num_cols + j
	mul $t2, $t2, $t1 #2*(i*num_cols+j)
	add $t2, $t2, $s0 #base address + 2*(i*num_cols+j)
	lh $t3, 0($t2)
	bne $t3, -1, valid
	addi $t0, $t0, 1 #j++
	blt $t0, $s1, colLop3
	move $v0, $s7
	j doneMerge
valid:
	addi $s7, $s7, 1 #non zero cell++
	addi $t0, $t0, 1 #j++
	blt $t0, $s1, colLop3
	move $v0, $s7
doneMerge:
	#addi $s5, $s5, 1 #i++
	#blt $s5, $s1, rowLoop1
        lw $s0, 0($sp)
	lw $s1, 4($sp)
	lw $s2, 8($sp)
	lw $s3, 12($sp)
	lw $s4, 16($sp)
	lw $s5, 20($sp)
	lw $s6, 24($sp)
	lw $s7, 28($sp)
	lw $ra, 32($sp)
	addi $sp, $sp, 36
    ############################################
    jr $ra

merge_col:
    #Define your code here
    ############################################
    #addi $sp, $sp, 4
    addi $sp, $sp, -36
    sw $s0, 0($sp)
    sw $s1, 4($sp)
    sw $s2, 8($sp)
    sw $s3, 12($sp)
    sw $s4, 16($sp)
    sw $s5, 20($sp)
    sw $s6, 24($sp)
    sw $s7, 28($sp)
    sw $ra, 32($sp)
    move $s0, $a0 #base address
    move $s1, $a1 #num_rows
    #addi $s1, $s1, -1 #rows-1
    move $s2, $a2 #num_cols
    move $s3, $a3 #col
    addi $sp, $sp, 36
    lw $s4, 0($sp) #direction
    addi $sp, $sp, -36
    addi $t8, $s2, -1
    bltz $s3, invalid4
    bgt $s3, $t8, invalid4
    blt $s1, 2, invalid4
    blt $s2, 2, invalid4
    li $t0, 2 #to multiply
    li $t6, -1 #for blank cell
    li $s5, 0 #row counter
    li $s7, 0 #non zero cells counter
    beqz $s4, rowLop1
    beq $s4, 1, rowLoop2
    j invalid4
rowLoop2:
	move $t1, $s3 #col counter
colLoop2:
	mul $t2, $s5, $s2 #i * num_columns
	add $t2, $t2, $t1 #i * num_columns + j
	mul $t2, $t2, $t0 #2*(i * num_columns + j)
	add $t2, $t2, $s0 #base_addr + 2*(i * num_columns + j)
	lh $t3, 0($t2) #load the number at arr[i][j]
	addi $t4, $s5, 1 #i+1
	mul $t4, $t4, $s2 #i * num_columns
	add $t4, $t4, $t1 #i * num_columns + j
	mul $t4, $t4, $t0 #2*(i * num_columns + j)
	add $t4, $t4, $s0 #base_addr + 2*(i * num_columns + j)
	lh $t5, 0($t4) #load the number at arr[i+1][j]
	beq $t3, $t5, mergeTop
	j doneMergeTop
mergeTop:
	bne $t3, -1, goAhead2
	j doneMergeTop
goAhead2:
	add $t3, $t3, $t5 #double the number
	sh $t3, 0($t2) #place it in arr[i][j]
	sh $t6, 0($t4) #place -1 in arr[i+1][j]
	j doneMergeTop
doneMergeTop:
	addi $s5, $s5, 1 #i++
	blt $s5, $s1, rowLoop2
	j colLoopDone2
rowLop1:
	addi $s5, $s1, -1 #row counter
colLop1:
	mul $t2, $s5, $s2 #i * num_columns
	add $t2, $t2, $s3 #i * num_columns + j
	mul $t2, $t2, $t0 #2*(i * num_columns + j)
	add $t2, $t2, $s0 #base_addr + 2*(i * num_columns + j)
	lh $t3, 0($t2) #load the number at arr[i][j]
	addi $t4, $s5, -1 #i-1
	mul $t4, $t4, $s2 #i * num_columns
	add $t4, $t4, $s3 #i * num_columns + j
	mul $t4, $t4, $t0 #2*(i * num_columns + j)
	add $t4, $t4, $s0 #base_addr + 2*(i * num_columns + j)
	lh $t5, 0($t4) #load the number at arr[i-1][j]
	beq $t3, $t5, mergeBottom
	j doneMergeBottom
mergeBottom:
	bne $t3, -1, goAhead3
	j doneMergeBottom
goAhead3:
	add $t3, $t3, $t5 #double the number
	sh $t3, 0($t2) #place it in arr[i][j]
	sh $t6, 0($t4) #place -1 in arr[i-1][j]
	j doneMergeBottom
doneMergeBottom:
	addi $s5, $s5, -1 #i--
	bgtz $s5, colLop1
	j colLoopDone2
invalid4:
	li $v0, -1
	j doneMerge1
colLoopDone2:
	li $t1, 0 #row counter
rowLop3:
	mul $t2, $t1, $s2
	add $t2, $t2, $s3
	mul $t2, $t2, $t0
	add $t2, $t2, $s0
	lh $t3, 0($t2)
	bne $t3, -1, valid1
	addi $t1, $t1, 1
	blt $t1, $s1, rowLop3
	move $v0, $s7
	j doneMerge1
valid1:
	addi $s7, $s7, 1 #non zero cells++
	addi $t1, $t1, 1 #i++
	blt $t1, $s1, rowLop3
	move $v0, $s7
doneMerge1:
	lw $s0, 0($sp)
	lw $s1, 4($sp)
	lw $s2, 8($sp)
	lw $s3, 12($sp)
	lw $s4, 16($sp)
	lw $s5, 20($sp)
	lw $s6, 24($sp)
	lw $s7, 28($sp)
	lw $ra, 32($sp)
	addi $sp, $sp, 36
    ############################################
    jr $ra

shift_row:
    #Define your code here
    ############################################
    #addi $sp, $sp, 4
    addi $sp, $sp, -36
    sw $s0, 0($sp)
    sw $s1, 4($sp)
    sw $s2, 8($sp)
    sw $s3, 12($sp)
    sw $s4, 16($sp)
    sw $s5, 20($sp)
    sw $s6, 24($sp)
    sw $s7, 28($sp)
    sw $ra, 32($sp)
    move $s0, $a0 #base address
    move $s1, $a1 #num_rows
    move $s2, $a2 #num_cols
    addi $t9, $s2, -1
    move $s3, $a3 #row
    addi $sp, $sp, 36
    lw $s4, 0($sp) #direction
    addi $sp, $sp, -36
    addi $t8, $s1, -1
    bltz $s3, invalid5
    bgt $s3, $t8, invalid5
    blt $s1, 2, invalid5
    blt $s2, 2, invalid5
    li $t1, 2 #to multiply
    li $t6, -1 #to place
    li $s7, 0 #cells shifted
    beqz $s4, rowLoop3
    beq $s4, 1, rowLoop4
    j invalid5
rowLoop3:
	li $s5, 0 #col counter
colLoop3:
	move $t3, $s5 #cancel that
	mul $t0, $s3, $s2 #i * num_columns
	add $t0, $t0, $s5 #i * num_columns + j
	mul $t0, $t0, $t1 #2*(i * num_columns + j)
	add $t0, $t0, $s0 #base_addr + 2*(i * num_columns + j)
	lh $t2, 0($t0)
	beq $t2, -1, shiftLeft
	addi $s5, $s5, 1 #j++
	blt $s5, $s2, colLoop3
	move $v0, $s7
	j doneShift
shiftLeft:
        blt $t3, $t9, contine
        move $v0, $s7
        j doneShift
contine:
	addi $t3, $t3, 1 #j+1
	mul $t4, $s3, $s2 #i * num_columns
	add $t4, $t4, $t3 #i * num_columns + j
	mul $t4, $t4, $t1 # 2*(i * num_columns + j)
	add $t4, $t4, $s0 #base_addr + 2*(i * num_columns + j)
	lh $t5, 0($t4) #number at arr[i][j+1]
	beq $t5, -1, shiftLeft
	sh $t5, 0($t0)
	sh $t6, 0($t4)
	addi $s7, $s7, 1 #cells shifted++
	addi $s5, $s5, 1 #j++
	blt $s5, $s2, colLoop3	
	move $v0, $s7
	j doneShift
rowLoop4:
	addi $s6, $s2, -1 #col#nu_cols-1
colLoop4:
	move $t3, $s6 #cancel that
	mul $t0, $s3, $s2 #i * num_columns
	add $t0, $t0, $s6 #i * num_columns + j
	mul $t0, $t0, $t1 #2*(i * num_columns + j)
	add $t0, $t0, $s0 #base_addr + 2*(i * num_columns + j)
	lh $t2, 0($t0)
	beq $t2, -1, shiftRight
	addi $s6, $s6, -1 #j--
	bgez $s6, colLoop4
	move $v0, $s7
	j doneShift
shiftRight:
	bgt $t3, 0, contine1
	move $v0, $s7
	j doneShift
contine1:
	addi $t3, $t3, -1 #j--
	mul $t4, $s3, $s2 #i * num_columns
	add $t4, $t4, $t3 #i * num_columns + j
	mul $t4, $t4, $t1 # 2*(i * num_columns + j)
	add $t4, $t4, $s0 #base_addr + 2*(i * num_columns + j)
	lh $t5, 0($t4) #number at arr[i][j-1]
	beq $t5, -1, shiftRight
	sh $t5, 0($t0)
	sh $t6, 0($t4)
	addi $s7, $s7, 1 #cells shifted++
	addi $s6, $s6, -1 #j--
	bgez $s6, colLoop4
	move $v0, $s7	
	j doneShift
invalid5:
	li $v0, -1
doneShift:
	lw $s0, 0($sp)
	lw $s1, 4($sp)
	lw $s2, 8($sp)
	lw $s3, 12($sp)
	lw $s4, 16($sp)
	lw $s5, 20($sp)
	lw $s6, 24($sp)
	lw $s7, 28($sp)
	lw $ra, 32($sp)
	addi $sp, $sp, 36
    ############################################
    jr $ra

shift_col:
    #Define your code here
    ############################################
    #addi $sp, $sp, 4
    addi $sp, $sp, -36
    sw $s0, 0($sp)
    sw $s1, 4($sp)
    sw $s2, 8($sp)
    sw $s3, 12($sp)
    sw $s4, 16($sp)
    sw $s5, 20($sp)
    sw $s6, 24($sp)
    sw $s7, 28($sp)
    sw $ra, 32($sp)
    move $s0, $a0 #base address
    move $s1, $a1 #num_rows
    addi $t9, $s1, -1
    move $s2, $a2 #num_cols
    move $s3, $a3 #col
    addi $sp, $sp, 36
    lw $s4, 0($sp) #direction
    addi $sp, $sp, -36
    addi $t8, $s2, -1
    bltz $s3, invalid6
    bgt $s3, $t8, invalid6
    blt $s1, 2, invalid6
    blt $s2, 2, invalid6
    li $s5, 0 #row counter
    li $t2, 2 #to multiply
    li $t6, -1 #to place
    li $s7, 0 #cells shifted
    beqz $s4, rowLoop5
    beq $s4, 1, rowLoop6
    j invalid6
rowLoop5:
	move $t0, $s3 #col
colLoop5:
	move $t4, $s5 #cancel that
	mul $t1, $s5, $s2 #i * num_columns
	add $t1, $t1, $s3 #i * num_columns + j
	mul $t1, $t1, $t2 #2*(i * num_columns + j)
	add $t1, $t1, $s0 #base_addr + 2*(i * num_columns + j)
	lh $t3, 0($t1)
	beq $t3, -1, shiftUp
	addi $s5, $s5, 1 #i++
	blt $s5, $s1, rowLoop5
	move $v0, $s7
	j doneShift1
shiftUp:
	blt $t4, $t9, contine2
	move $v0, $s7
	j doneShift1
contine2:
	addi $t4, $t4, 1 #i++
	mul $t5, $t4, $s2 #i * num_columns
	add $t5, $t5, $s3 #i * num_columns + j
	mul $t5, $t5, $t2 #2*(i * num_columns + j)
	add $t5, $t5, $s0 #base_addr + 2*(i * num_columns + j)
	lh $t7, 0($t5)
	beq $t7, -1, shiftUp
	sh $t7, 0($t1)
	sh $t6, 0($t5)
	addi $s7, $s7, 1 #cells shifted++
	addi $s5, $s5, 1 #i++
	blt $s5, $s1, rowLoop5
	move $v0, $s7
	j doneShift1
rowLoop6:
	addi $s6, $s1, -1 #row#num_rows-1
colLoop6:
	move $t3, $s6 #cancel that
	mul $t0, $s6, $s2 #i * num_columns
	add $t0, $t0, $s3 #i * num_columns + j
	mul $t0, $t0, $t2 #2*(i * num_columns + j)
	add $t0, $t0, $s0 #base_addr + 2*(i * num_columns + j)
	lh $t1, 0($t0) 
	beq $t1, -1, shiftDown
	addi $s6, $s6, -1 #i--
	bgez $s6, colLoop6
	move $v0, $s7
	j doneShift1
shiftDown:
	bgt $t3, 0, contine3
	move $v0, $s7
	j doneShift1
contine3:
	addi $t3, $t3, -1 #i--
	mul $t4, $t3, $s2 #i * num_columns
	add $t4, $t4, $s3 #i * num_columns + j
	mul $t4, $t4, $t2 #2*(i * num_columns + j)
	add $t4, $t4, $s0 #base_addr + 2*(i * num_columns + j)
	lh $t5, 0($t4) 
	beq $t5, -1, shiftDown
	sh $t5, 0($t0)
	sh $t6, 0($t4)
	addi $s7, $s7, 1
	addi $s6, $s6, -1 #i--
	bgez $s6, colLoop6
	move $v0, $s7
	j doneShift1
invalid6:
	li $v0, -1
doneShift1:
	lw $s0, 0($sp)
	lw $s1, 4($sp)
	lw $s2, 8($sp)
	lw $s3, 12($sp)
	lw $s4, 16($sp)
	lw $s5, 20($sp)
	lw $s6, 24($sp)
	lw $s7, 28($sp)
	lw $ra, 32($sp)
	addi $sp, $sp, 36
    ############################################
    jr $ra

check_state:
    #Define your code here
    ############################################
    addi $sp, $sp, -24
    sw $s0, 0($sp)
    sw $s1, 4($sp)
    sw $s2, 8($sp)
    sw $s3, 12($sp)
    sw $s4, 16($sp)
    sw $ra, 20($sp)
    move $s0, $a0 #base address
    move $s1, $a1 #num_rows
    move $s2, $a2 #num_cols
    li $s3, 0 #counter
    li $t2, 2 #to multiply
    mul $s4, $s1, $s2
    #addi $s4, $s4, -1
    move $t1, $s0
loop:
    lh $t0, 0($t1)
    bne $t0, -1, noEmpty
    j checkWin
noEmpty:
    addi $t1, $t1, 2 #increment by 2
    addi $s3, $s3, 1
    blt $s3, $s4, loop
checkMerge:
    li $s3, 0 #row counter
rowLoop11:
    li $s4, 0 #col counter
colLoop11:
    mul $t0, $s3, $s2 #i * num_columns
    add $t0, $t0, $s4 #i * num_columns + j
    mul $t0, $t0, $t2 #2*(i * num_columns + j)
    add $t0, $t0, $s0 #base_addr + 2*(i * num_columns + j)
    lh $t1, 0($t0) #number at arr[i][j]
    addi $t3, $s3, -1
    bltz $t3, continueAhead
    #bgt $t3, $s1, contineAhead
    mul $t4, $t3, $s2 #i * num_columns
    add $t4, $t4, $s4 #i * num_columns + j
    mul $t4, $t4, $t2 #2*(i * num_columns + j)
    add $t4, $t4, $s0 #base_addr + 2*(i * num_columns + j)
    lh $t5, 0($t4) #number at arr[i-1][j]
    beq $t1, $t5, checkWin
continueAhead:
    addi $t3, $s3, 1
    bge $t3, $s1, continueAhead1
    mul $t4, $t3, $s2 #i * num_columns
    add $t4, $t4, $s4 #i * num_columns + j
    mul $t4, $t4, $t2 #2*(i * num_columns + j)
    add $t4, $t4, $s0 #base_addr + 2*(i * num_columns + j)
    lh $t5, 0($t4) #number at arr[i-1][j]
    beq $t1, $t5, checkWin
continueAhead1:
    addi $t3, $s4, -1
    bltz $t3, continueAhead2
    mul $t4, $s3, $s2 #i * num_columns
    add $t4, $t4, $t3 #i * num_columns + j
    mul $t4, $t4, $t2 #2*(i * num_columns + j)
    add $t4, $t4, $s0 #base_addr + 2*(i * num_columns + j)
    lh $t5, 0($t4) #number at arr[i][j-1]
    beq $t1, $t5, checkWin
continueAhead2:
    addi $t3, $s4, 1
    bge $t3, $s2, continueAhead3
    mul $t4, $s3, $s2 #i * num_columns
    add $t4, $t4, $t3 #i * num_columns + j
    mul $t4, $t4, $t2 #2*(i * num_columns + j)
    add $t4, $t4, $s0 #base_addr + 2*(i * num_columns + j)
    lh $t5, 0($t4) #number at arr[i][j-1]
    beq $t1, $t5, checkWin
continueAhead3:
    addi $s4, $s4, 1 #j++
    blt $s4, $s2, colLoop11
colLoopDone3:
    addi $s3, $s3, 1
    blt $s3, $s1, rowLoop11
    j gameLost
checkWin:
    li $s3, 0 #counter
    move $t0, $s0
loop3:
    lh $t1, 0($t0)
    bge $t1, 2048, gameWon
    addi $s3, $s3, 1
    addi $t0, $t0, 2
    blt $s3, $s4, loop3
    j stalemate
gameWon:
    li $v0, 1
    j doneCheckState
gameLost:
    li $v0, -1
    j doneCheckState
stalemate:
    li $v0, 0
doneCheckState:
    lw $s0, 0($sp)
    lw $s1, 4($sp)
    lw $s2, 8($sp)
    lw $s3, 12($sp)
    lw $s4, 16($sp)
    lw $ra, 20($sp)
    addi $sp, $sp, 24
    ############################################
    jr $ra

user_move:
    #Define your code here
    ############################################
    addi $sp, $sp, -24
    sw $s0, 0($sp)
    sw $s1, 4($sp)
    sw $s2, 8($sp)
    sw $s3, 12($sp)
    sw $s4, 16($sp)
    sw $ra, 20($sp)
    move $s0, $a0 #base address
    move $s1, $a1 #num_rows
    move $s2, $a2 #num_cols
    move $s3, $a3 #direction
    li $t0, 0 #for left and up
    li $t1, 1 #for right and down
    beq $s3, 'L', dirLeft
    beq $s3, 'R', dirRight
    beq $s3, 'U', dirUp
    beq $s3, 'D', dirDown
    j invalid7
dirLeft:
    li $s4, 0 #row counter
rowLoop9:
    move $a3, $s4 #row#
    addi $sp, $sp, -4
    sw $t0, 0($sp)
    jal shift_row
    addi $sp, $sp, 4
    beq $v0, -1, invalid7
    li $t0, 0
    
    move $a3, $s4
    addi $sp, $sp, -4
    sw $t0, 0($sp)
    jal merge_row
    addi $sp, $sp, 4
    beq $v0, -1, invalid7
    li $t0, 0
    
    move $a3, $s4
    addi $sp, $sp, -4
    sw $t0, 0($sp)
    jal shift_row
    addi $sp, $sp, 4
    beq $v0, -1, invalid7
    li $t0, 0
    
    addi $s4, $s4, 1
    blt $s4, $s1, rowLoop9
    jal check_state
    move $v1, $v0
    li $v0, 0
    j doneUserMove
dirRight:
    li $s4, 0 #row counter
rowLoop10:
    move $a3, $s4 #row#
    addi $sp, $sp, -4
    sw $t1, 0($sp)
    jal shift_row
    addi $sp, $sp, 4
    beq $v0, -1, invalid7
    li $t1, 1
    
    move $a3, $s4
    addi $sp, $sp, -4
    sw $t1, 0($sp)
    jal merge_row
    addi $sp, $sp, 4
    beq $v0, -1, invalid7
    li $t1, 1
    
    move $a3, $s4
    addi $sp, $sp, -4
    sw $t1, 0($sp)
    jal shift_row
    addi $sp, $sp, 4
    beq $v0, -1, invalid7
    li $t1, 1
    
    addi $s4, $s4, 1
    blt $s4, $s1, rowLoop10
    jal check_state
    move $v1, $v0
    li $v0, 1
    j doneUserMove
dirUp:
    li $s4, 0 #col counter
colLoop9:
    move $a3, $s4 #col#
    addi $sp, $sp, -4
    sw $t0, 0($sp)
    jal shift_col
    addi $sp, $sp, 4
    beq $v0, -1, invalid7
    li $t1, 1
    
    move $a3, $s4
    addi $sp, $sp, -4
    sw $t1, 0($sp)
    jal merge_col
    addi $sp, $sp, 4
    beq $v0, -1, invalid7
    li $t0, 0
    
    move $a3, $s4
    addi $sp, $sp, -4
    sw $t0, 0($sp)
    jal shift_col
    addi $sp, $sp, 4
    beq $v0, -1, invalid7
    li $t0, 0
    
    addi $s4, $s4, 1 #j++
    blt $s4, $s2, colLoop9
    jal check_state
    move $v1, $v0
    li $v0, 0
    j doneUserMove
dirDown:
    li $s4, 0 #col counter
colLoop10:
    move $a3, $s4 #col#
    addi $sp, $sp, -4
    sw $t1, 0($sp)
    jal shift_col
    addi $sp, $sp, 4
    beq $v0, -1, invalid7
    li $t0, 0
    
    move $a3, $s4
    addi $sp, $sp, -4
    sw $t0, 0($sp)
    jal merge_col
    addi $sp, $sp, 4
    beq $v0, -1, invalid7
    li $t1, 1
    
    move $a3, $s4
    addi $sp, $sp, -4
    sw $t1, 0($sp)
    jal shift_col
    addi $sp, $sp, 4
    beq $v0, -1, invalid7
    li $t1, 1
    
    addi $s4, $s4, 1 #j++
    blt $s4, $s2, colLoop10
    jal check_state
    move $v1, $v0
    li $v0, 1
    j doneUserMove
invalid7:
    li $v0, -1
    li $v1, -1
doneUserMove:
    lw $s0, 0($sp)
    lw $s1, 4($sp)
    lw $s2, 8($sp)
    lw $s3, 12($sp)
    lw $s4, 16($sp)
    lw $ra, 20($sp)
    addi $sp, $sp, 24
    ############################################
    jr $ra

#################################################################
# Student defined data section
#################################################################
.data
.align 2  # Align next items to word boundary

#place all data declarations here


