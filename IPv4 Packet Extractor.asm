##############################################################
# Homework #3
# name: Janmejay Bhavsar
# sbuid: 110476847	
##############################################################

.text
replace1st:
	
    #Define your code here
	############################################
	addi $sp, $sp, -16
	sw $ra, 0($sp)
	sw $s0, 4($sp)
	sw $s1, 8($sp)
	sw $s2, 12($sp)
	move $s0, $a0	#store the val of the char array
	move $s1, $a1	#store the toReplace arg
	move $s2, $a2	#store the replaceWith arg
	li $t4, 0
	li $t5, 127
	blt $s1, $t4, errorReplace
	bgt $s1, $t5, errorReplace
	blt $s2, $t4, errorReplace
	bgt $s2, $t5, errorReplace
	j loopConditionals
errorReplace:
	li $v0, -1
	j done
loopConditionals:
	lb $t0, 0($s0)
	bnez $t0, loopLetter
	li $v0, 0
	j done
loopLetter:
	beq $t0, $s1, foundLetter
	j didNotFindLetter
foundLetter:
	sb $s2, 0($s0)
	addi $s0, $s0, 1
	move $v0, $s0
	j done
didNotFindLetter:
	addi $s0, $s0, 1
	j loopConditionals
	
	############################################
done:
	lw $ra, 0($sp)
	lw $s0, 4($sp)
	lw $s1, 8($sp)
	lw $s2, 12($sp)
	addi $sp, $sp, 16
	jr $ra

printStringArray:
    #Define your code here
	############################################
	addi $sp, $sp, -20
	sw $ra, 0($sp)
	sw $s0, 4($sp)
	sw $s1, 8($sp)
	sw $s2, 12($sp)
	sw $s3, 16($sp)
	move $s0, $a0	#store the starting address of string array
	move $s1, $a1	#store the start index
	move $s2, $a2	#store the end index 
	move $s3, $a3	#store the length
	li $t4, 0	#loop counter
	li $t6, 4
	li $t0, 1
	li $t2, 0
	blt $s3, $t0, errorPrint
	blt $s1, $t2, errorPrint
	blt $s2, $t2, errorPrint
	bge $s1, $s3, errorPrint
	bge $s2, $s3, errorPrint
	blt $s2, $s1, errorPrint
	mul $t6, $t6, $s1
	add $s0, $s0, $t6
	j loopConditional
errorPrint:
	li $v0, -1
	j doneString
loopConditional:
	ble $s1, $s2, stringLoop
	bgt $s1, $s2, doneString
stringLoop:
	li $t5, 0
	lw $t5, 0($s0)
	move $a0, $t5
	li $v0, 4
	syscall
	li $v0, 4
	la $a0, newLine
	syscall
	li $v0, 4
	la $a0, newLine
	syscall
	addi $s0, $s0, 4
	addi $t4, $t4, 1
	addi $s1, $s1, 1
	j loopConditional
doneString:
	move $v0, $t4
	############################################
	lw $ra, 0($sp)
	lw $s0, 4($sp)
	lw $s1, 8($sp)
	lw $s2, 12($sp)
	lw $s3, 16($sp)
	addi $sp, $sp, 20
    jr $ra
verifyIPv4Checksum:
    #Define your code here
	############################################
	addi $sp, $sp, -20
	sw $ra, 0($sp)
	sw $s0, 4($sp)
	sw $s6, 8($sp)
	sw $t0, 12($sp)
	sw $t2, 16($sp)
	move $s0, $a0
	li $s6, 0
	lb $t1, 3($s0)
	andi $t2, $t1, 15
	li $t3, 2
	mult $t2, $t3
	mflo $t2
	li $t3, 0
loopCondition:
	blt $t3, $t2, addLoop	#check if the starting address is less than the ending address
	j endLoop
	
addLoop:
	lhu $t5, 0($s0)	
	add $s6, $t5, $s6	#load the 0th halfword
	addi $t3, $t3, 1
	addi $s0, $s0,2
	j loopCondition
endLoop: 
	li $t8, 65535
	bgt $s6, $t8, endAroundCarry
	j end
endAroundCarry:
#lhu $t7, 0($s6)
	andi $t7, $s6, 458752
	srl $t7, $t7, 16
	andi $s6, $s6, 65535
	#sll $s6, $s6, 3
	#srl $s6, $s6, 3
	add $s6, $s6, $t7
	j endLoop
end:
  	not $s6, $s6
  	andi $s6, $s6, 65535
  	beqz $s6, checkSumValid
  	bnez $s6, checkSumInvalid
checkSumValid:
	li $v0, 0
	j doneVerification
checkSumInvalid:
	move $v0, $s6
	j doneVerification
	############################################
doneVerification:
	lw $ra, 0($sp)
	lw $s0, 4($sp)
	lw $s6, 8($sp)
	lw $t0, 12($sp)
	lw $t2, 16($sp)
	addi $sp, $sp, 20
    jr $ra
    
extractUnorderedData:
	#Define your code here
	############################################
	addi $sp, $sp, -36
	sw $s0, 0($sp)
	sw $s1, 4($sp)
	sw $s2, 8($sp)
	sw $s3, 12($sp)
	sw $s4, 16($sp)
	sw $s5, 20($sp)
	sw $s6, 24($sp)
	sw $s7, 28($sp)
	sw $ra, 32($sp)
	move $s0, $a0
	move $s1, $a1
	move $s2, $a2
	move $s3, $a3
	li $s5, 0
	li $t9, 0 #packet counter
	li $t0, 0 #constant value
	li $t2, 0 #constant value
packetChecker:
	move $a0, $s0
	jal verifyIPv4Checksum
	li $t5, 4
	#li $s5, 0 #constant value
	li $s6, 0 #constant value
	beqz $v0, packetValid
	bnez $v0, packetInvalid
packetValid:
	lh $t1, 0($s0)	#total lenght recieved
	bgt $t1, $s3, packetInvalid
	lb $t3, 3($s0) #for the header lenght
	andi $t3, $t3, 15	#got the header llength
	mult $t3, $t5	#multiply by 4
	mflo $t3	#store it in t3
	sub $t4, $t1, $t3 #got the size of the payload
	#add $t0, $t0, $t4
	lhu $t5, 4($s0)	#for the flag
	andi $t6, $t5, 57344 
	srl $t6, $t6, 13 #got the flag
	andi $t7, $t5, 8191 #got the fragment offset
	beq $s1, 1, checkFlagPacket1 #check flag for 1 packet
	j StartingPacketCheck
checkFlagPacket1:
	beq $t6, 4, Fail	#check for 1 packet
	beqz $t6, checkFragmentOffsetPacket1	#check FO for 1 packet
	beq $t6, $t2, checkFragmentOffsetPacket1
	beqz $t6, checkFragmentOffsetPacket1
	beq $t6, 2, startStore
checkFragmentOffsetPacket1:
	bnez $t7, Fail
	beqz $t7, startStore
	bnez $t7, startStoreDifferent
StartingPacketCheck:
	beq $t6, 4, checkFO
	j EndingPacketCheck
checkFO:
	beqz $t7, gotStartingPacket
	j EndingPacketCheck
gotStartingPacket:
	addi $t0, $t0, 1
EndingPacketCheck:
	beqz $t6, checkFO1
	j checkPacketFlagMoreThan1
checkFO1:
	bnez $t7, gotEndingPacket
	j checkPacketFlagMoreThan1
gotEndingPacket:
	addi $t2, $t2, 1
checkPacketFlagMoreThan1:
	beq $t6, 2, Fail #check for  more than 1 packet
	beq $t6, 4, checkFragmentOffsetPacketMoreThan1	
	beqz $t6, checkFragmentOffsetPacketMoreThan1Alt
	j checkFragmentOffsetPacketMoreThan1
checkFragmentOffsetPacketMoreThan1Alt:
	beqz $t7, Fail
	#beq $t6, 2, startStore
checkFragmentOffsetPacketMoreThan1:
	beqz $t7, startStore
	bnez $t7, startStoreDifferent
startStore:
	move $s6, $t4
	add $s0, $s0, $t3 #increment by the header length to get access to the first byte of the payload
loopStore:
	lb $t8, 0($s0)	#load byte by byte the payload
	sb $t8, 0($s2)	#store it byte by byte
	#move $s5, $s2 #max index finder
	addi $s6, $s6, -1 #decrement payload size by 1
	addi $s0, $s0, 1 #increment s0 by 1
	addi $s2, $s2, 1 #increment s2 by 1
	bgt $s2, $s5, storeMaxIndex
	j noMax
storeMaxIndex:
	move $s5, $s2
noMax:
	#addi $t0, $t0, 1 #increment the number of bytes stored
	bnez $s6, loopStore #until payload size is 0 continue storing 
	addi $t9, $t9, 1 #after stroring the payload increment the packet counter
	add $s4, $t3, $t4 #add the size of the payload and the header length
	sub $s0 $s0, $s4 #do this to go to the start of the packet
	blt $t9, $s1, nextPacket
	beq $s1, 1, noCheck
	beqz $t0, Fail
	bgt $t0, 1, Fail
	beqz $t2, Fail
	bgt $t2, 1, Fail
noCheck:
	sub $s5, $s5, $a2
	li $v0, 0
	move $v1, $s5
	j doneExtract
startStoreDifferent:
	move $s6, $t4
	add $s0, $s0, $t3 #increment by the header length to get access to the first byte of the payload
	move $s2, $a2
	add $s2, $s2, $t7 #since we need to store in msg[Fragment Offset]
loopStoreDifferent:
	lb $t8, 0($s0)	#load byte by byte the payload
	sb $t8, 0($s2)	#store it byte by byte
	#move $s5, $s2 #max index finder
	addi $s6, $s6, -1 #decrement payload size by 1
	addi $s0, $s0, 1 #increment s0 by 1
	addi $s2, $s2, 1 #increment s2 by 1
	bgt $s2, $s5, storeMaxIndexDifferent
	j noMaxDifferent
storeMaxIndexDifferent:
	move $s5, $s2
noMaxDifferent:
	#addi $t0, $t0, 1 #increment the number of bytes stored
	bnez $s6, loopStoreDifferent #until payload size is 0 continue storing 
	addi $t9, $t9, 1 #after stroring the payload increment the packet counter
	add $s4, $t3, $t4 #add the size of the payload and the header length
	sub $s0 $s0, $s4 #do this to go to the start of the packet
	add $s7, $t4, $t7 #we moved ahead this much to store the complete msg
	sub $s2, $s2, $s7 #we again go to the start of the msg array
	blt $t9, $s1, nextPacket
	beq $s1, 1, noCheck1
	beqz $t0, Fail
	bgt $t0, 1, Fail
	beqz $t2, Fail
	bgt $t2, 1, Fail
noCheck1:
	li $v0, 0
	sub $s5, $s5, $a2
	move $v1, $s5
	j doneExtract
nextPacket:
	add $s0, $s0, $s3 #add the packet entry size to the next packet in the array
	j packetChecker
packetInvalid:
	li $v0, -1
	move $v1, $t9
	j doneExtract
Fail:
	li $v0, -1
	li $v1, -1
	j doneExtract
doneExtract: 
	lw $s0, 0($sp)
	lw $s1, 4($sp)
	lw $s2, 8($sp)
	lw $s3, 12($sp)
	lw $s4, 16($sp)
	lw $s5, 20($sp)
	lw $s6, 24($sp)
	lw $s7, 28($sp)
	lw $ra, 32($sp)
	addi $sp, $sp, 36
	jr $ra
############################################
processDatagram:
    #Define your code here
	############################################
	addi $sp, $sp, -20
	sw $ra, 0($sp)
	sw $s0, 4($sp)
	sw $s1, 8($sp)
	sw $s2, 12($sp)
	sw $s3, 16($sp)
	move $s0, $a0
	move $s1, $a1
	move $s2, $a2
	blez $s1, errorData
	li $t1, 0
	sw $s0, 0($s2) #we always need to store the starting address of the String to print
	addi $s2, $s2, 4 # we then increment the array by 4
	addi $t1, $t1, 1
	add $s0, $s0, $s1 #increment to the position M
	lb $t0, 0($s0) #the char to be replaced which is position M
	move $a0, $s0
	move $a1, $t0
	li $a2, '\0'
	jal replace1st
	sub $s0, $s0, $s1 #decrement the stating address of String to get to the start of the array
replaceLoopCondition:
	lb $t0, 0($s0)
	bne $t0, '\0', replaceLoop
	j doneData
replaceLoop:
	beq $t0, '\n', replace
	addi $s0, $s0, 1
	j replaceLoopCondition
replace:
	move $a0, $s0
	li $a1, '\n'
	li $a2, '\0'
	jal replace1st
	sw $v0, 0($s2)
	addi $s2, $s2, 4
	addi $t1, $t1, 1
	move $s0, $v0
	j replaceLoopCondition
errorData:
	li $v0, -1
	j doneProcess
doneData:
	move $v0, $t1
	############################################
doneProcess:
	lw $ra, 0($sp)
	lw $s0, 4($sp)
	lw $s1, 8($sp)
	lw $s2, 12($sp)
	lw $s3, 16($sp)
	addi $sp, $sp, 20
    jr $ra
    
printUnorderedDatagram:
	lw $s4, 0($sp)
	addi $sp, $sp, 4
	addi $sp, $sp, -24
	sw $ra, 0($sp)
	sw $s0, 4($sp)
	sw $s1, 8($sp)
	sw $s2, 12($sp)
	sw $s3, 16($sp)
	sw $s4, 20($sp)
	move $s0, $a0
	move $s1, $a1
	move $s2, $a2
	move $s3, $a3
	move $a3, $s4
	jal extractUnorderedData
	move $t0, $v0
        move $t1, $v1
        beqz $t0, proceed
        j invalidDatagram
proceed:
	move $a0, $s2
	move $a1, $t1
	move $a2, $s3
	jal processDatagram
	move $a0, $s3
	li $a1, 0
	addi $v0, $v0, -1
	move $a2, $v0
	addi $v0, $v0, 1
	move $a3, $v0
	jal printStringArray
	li $v0, 0
	j donePrint
invalidDatagram:
	li $v0, -1	
	j donePrint
    ############################################
donePrint:
    lw $ra, 0($sp)
    lw $s0, 4($sp)
    lw $s1, 8($sp)
    lw $s2, 12($sp)
    lw $s3, 16($sp)
    lw $s4, 20($sp)
    addi $sp, $sp, 24
    jr $ra
#################################################################
# Student defined data section
#################################################################
.data
.align 2  # Align next items to word boundary
m: .asciiz "m:"
n: .asciiz "n:"
comma: .asciiz ","
newLine: .asciiz "\n"
#place all data declarations here
